'''
Created on 1 Mar 2017

@author: Wai Get
'''

import time
import math
import draw
import calcArray
import calcSinkArray
import getIdentity
import calcVerticeSinkArray

if __name__ == '__main__':
    
    ##############
    ## SETTINGS ##
    ##############
    
    blocksize = 80  # pixel size of each colored block (image may become very large)
    
    sandInit = 2500000
    terminateAt = 100
    
    length = 301 # please keep this odd, setting for sink matrix
    
    n = 11 # size of identity matrix - please keep odd too due to way its coded
    
    identity = True
    sink = False    # run the sink code
    sinkVertice = False
    nosink = False
    save = True #save to img
    displayimg = True   #display image
    animate = False #animate (for the infinite matrix)
    
    ##################
    ## END SETTINGS ##
    ##################
    

    
    start_time = time.time()
    
    
    
    sandEnd = [[0],[0,0]]
    
    if sink == True:
        folder = "sink/"
        cursand = sandInit
        sandEnd = calcSinkArray.getArray(sandInit,length, sandEnd)
        print("Matrix calculated in --- %s seconds ---" % (time.time() - start_time))
        
        screensize = math.floor(len(sandEnd)*2.1*blocksize)
        drawtime = time.time()
        draw.draw(screensize, blocksize, sandEnd, sandInit, save, displayimg,cursand, terminateAt, folder)
        print("Image drawn in --- %s seconds ---" % (time.time() - drawtime))
        print("")
        
    if sinkVertice == True:
        folder = "sinkVertice/"
        cursand = sandInit
        sandEnd = calcVerticeSinkArray.getArray(sandInit,length, sandEnd)
        print("Matrix calculated in --- %s seconds ---" % (time.time() - start_time))
        
        screensize = math.floor(len(sandEnd)*2.1*blocksize)
        drawtime = time.time()
        draw.draw(screensize, blocksize, sandEnd, sandInit, save, displayimg,cursand, terminateAt, folder)
        print("Image drawn in --- %s seconds ---" % (time.time() - drawtime))
        print("")
        
    if nosink == True:
        
        if animate == True:
            folder = "animate/"
            counter = 1
            for x in range (0,terminateAt,sandInit):
                
                cursand = x+sandInit
                sandEnd = calcArray.getArray(sandInit, sandEnd, folder)
                print("Step "+str(counter)+" of " + str(terminateAt//sandInit))
                counter+=1
                screensize = math.floor(len(sandEnd)*2.1*blocksize)
                
                draw.draw(screensize, blocksize, sandEnd, sandInit, save, displayimg,cursand, terminateAt, folder)
            
        else:
            folder = "nosink/"
            cursand = sandInit
            sandEnd = calcArray.getArray(sandInit, sandEnd, folder)
            print("")
            print("Matrix calculated in --- %s seconds ---" % (time.time() - start_time))
            screensize = math.floor(len(sandEnd)*2.1*blocksize)
            
            drawtime = time.time()
            draw.draw(screensize, blocksize, sandEnd, sandInit, save, displayimg,cursand, terminateAt, folder)
            print("Image drawn in --- %s seconds ---" % (time.time() - drawtime))
            print("")
            
    if identity == True:
        folder = "identity/"
        screensize = max((math.floor((n*blocksize)*1.05)),((n+2)*blocksize))
        cursand = n
        sandEnd = []
        
        sandEnd = getIdentity.getArray(n,sandEnd)
        
        print("50% done in" + " --- %s seconds ---" % (time.time() - start_time))
        
        sandEnd = getIdentity.getArray(n,sandEnd)
        
        print("Identity matrix calculated in --- %s seconds ---" % (time.time() - start_time))
        
        drawtime = time.time()
        
        draw.draw(screensize, blocksize, sandEnd, sandInit, save, displayimg,cursand, terminateAt, folder)
        print("Image drawn in --- %s seconds ---" % (time.time() - drawtime))
        print("")
        
    
    

        
        
        
    print("Completed in --- %s seconds ---" % (time.time() - start_time))  
    
    if save == True:
        print("")
        print("Image(s) saved to "+folder[:-1]+" folder.")
    else:
        print("")
        print("Image(s) not saved.")
        
    
    input("Press enter to exit ;)")