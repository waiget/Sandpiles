'''
Created on 1 Mar 2017

@author: Wai Get
'''
import math
import time


def getArray(sand, elements, folder):
    
    
    maxarr = len(elements)
    
    starttime = time.time()

    x = sand + elements[0][0]
    
    
    
    elements[0][0] = x%4
    elements[1][0] += x//4
    max = 4
    linescode = 0
    percentshow = 5
    while max>3:
        
        if folder =="nosink/":
            
            totalblocks = (len(elements))*(len(elements))*math.pi*2.1
            percentprogress = (totalblocks/sand)*100
            
            if percentprogress>percentshow:
                print("Progress is about "+str(percentprogress)[0:5]+"% complete. Time since last check:"+" --- %s seconds ---"% (time.time() - starttime))
                starttime = time.time()
                percentshow +=5
        
        max = 0
        for y in range (1,maxarr):
            for x in range(y+1):
                linescode+=1
                
                #print(str(x)+" "+str(y))
                if elements[y][x]>max:
                    
                    max = elements[y][x]
                
                if elements[y][x]>3:
                    #print("x and y are: "+str(x)+str(y))
                    #print(elements)
                    
                    #special case for 0,1:
                    if x == 0 and y ==1:
                        timeoffour = 0
                        while elements[y][x]>3:
                            timeoffour += elements[y][x]//4
                            elements[y][x] = elements[y][x]%4 + elements[y][x]//4
                            #print("new 01: "+str(elements[y][x])+" time of four "+str(timeoffour))
                        
                        if maxarr<3:
                            elements.append([timeoffour,0,0])
                            maxarr+=1
                        else:
                            elements[y+1][x] += timeoffour
                        
                        elements[y][x+1] += timeoffour*2
                        # works to here
                        
                    elif x ==0:
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        
                        if maxarr<y+2:
                            maxarr+=1
                            elements.append([0]*(y+2))
                            
                        elements[y+1][x] += timeoffour
                        elements[y][x+1] += timeoffour
                        elements[y-1][x] += timeoffour
                    
                    elif x == y:
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        elements[y+1][x] +=timeoffour
                        
                        if (x-1)==0:
                            elements[y][x-1] +=timeoffour*2
                        else:
                            elements[y][x-1] += timeoffour
                    
                    else:
                        
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        
                        
                        
                        if x ==1:
                            elements[y][0] += timeoffour*2
                            
                        else:
                            elements[y][x-1] += timeoffour
                            
                        
                        if (y-1)==x:
                            
                            elements[x][x] += timeoffour*2
                            elements[y][y] +=timeoffour*2
                        else:
                            elements[y-1][x] += timeoffour
                            elements[y][x+1] += timeoffour
                        
                        if maxarr<y+2:
                            maxarr+=1
                            elements.append([0]*(y+2))
                        elements[y+1][x] +=timeoffour
                        
                            
                            
                            

    
#     elements.append([3,4,5])
#     elements[3].append(1)
    
    #print (elements[3][3])
    
    #print(linescode)
    
    


    return elements
        