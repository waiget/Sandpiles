'''
Created on 1 Mar 2017

@author: Wai Get
'''
import math


def getArray(sand, length, elements):
    
    radius = (length+1)//2
    #print(radius)
    
    maxarr = len(elements)


    x = sand + elements[0][0]
    
    
    
    elements[0][0] = x%4
    elements[1][0] += x//4
    
    
    max = 4
    linescode = 0
    
    while max>3:
        
        max = 0
        for y in range (1,maxarr):
            for x in range(y+1):
                linescode+=1
                
                #print(str(x)+" "+str(y))
                if elements[y][x]>max:
                    
                    max = elements[y][x]
                
                if elements[y][x]>3:
                    
                    #print(elements)
                    #print("x and y are: "+str(x)+str(y))
                    
                    #special case for 0,1:
                    if x == 0 and y ==1:
                        timeoffour = 0
                        while elements[y][x]>3:
                            timeoffour += elements[y][x]//4
                            elements[y][x] = elements[y][x]%4 + elements[y][x]//4
                            #print("new 01: "+str(elements[y][x])+" time of four "+str(timeoffour))
                        
                        if maxarr<3 and maxarr<radius:
                            elements.append([timeoffour,0,0])
                            maxarr+=1
                        elif (y+1)<radius:
                            elements[y+1][x] += timeoffour
                        
                            
                        
                        elements[y][x+1] += timeoffour*2
                        # works to here
                        
                    elif x ==0:
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        
                        if maxarr<y+2 and maxarr<radius:
                            maxarr+=1
                            elements.append([0]*(y+2))
                            
                        if (y+1)<radius:
                            elements[y+1][x] += timeoffour
                            
                        elements[y][x+1] += timeoffour
                        elements[y-1][x] += timeoffour
                    
                    elif x == y:
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        
                        if (y+1)<radius:
                            elements[y+1][x] +=timeoffour
                        
                        if (x-1)==0:
                            elements[y][x-1] +=timeoffour*2
                        else:
                            elements[y][x-1] += timeoffour
                            
                            ##should be fine to here kappa
                    
                    else:
                        
                        timeoffour = elements[y][x]//4
                        elements[y][x] = elements[y][x]%4
                        
                        
                        
                        if x ==1:
                            elements[y][0] += timeoffour*2
                            
                        else:
                            elements[y][x-1] += timeoffour
                            
                        
                        if (y-1)==x:
                            
                            elements[x][x] += timeoffour*2
                            elements[y][y] +=timeoffour*2
                        else:
                            elements[y-1][x] += timeoffour
                            elements[y][x+1] += timeoffour
                        
                        if maxarr<y+2 and maxarr<radius:
                            #print("did it at "+str(y))
                            maxarr+=1
                            elements.append([0]*(y+2))
                        
                        
                        if(y+1)<radius:
                            
                            elements[y+1][x] +=timeoffour
                        
                            
                            
                            

    
#     elements.append([3,4,5])
#     elements[3].append(1)
    
    #print (elements[3][3])
    
    #print(linescode)
    
    


    return elements
        