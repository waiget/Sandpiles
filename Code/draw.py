'''
Created on 1 Mar 2017

@author: Wai Get
'''

import math
import random
import pygame
import sysconfig



#from pygame.locals import*

#pygame.display.init()

def draw(screensize, blocksize, array, sandInit, save, displayimg, cursand, terminateAt, folder):
    
    
    

    
    
    
    
    screen = pygame.display.set_mode((screensize,screensize),pygame.RESIZABLE)
    screen.fill((255,255,255))
    
    red = (255,0,0) # is 3
    green = (0,255,0) # is 2
    blue = (0,0,255) #is 1
    #yellow = (255,255,0)
    
    radius = len(array)
    
    #print(blocksize)
    
    center = math.floor(screensize/2)
    
    orig = [center-math.floor(blocksize/2),center-math.floor(blocksize/2)]
    
    #print(orig)


    
    for y in range (radius):
        for x in range (len(array[y])):
            
            #print("x "+str(x)+" y "+str(y))
            #print(array[y][x])

             
            if array[y][x] == 1:
                color = blue
            elif array[y][x] == 2:
                color = green
            elif array[y][x] == 3:
                color = red
            else:
                color = (255,255,255)
                
                
            #(startx, starty, lengthx, lengthy)
            

            #draw first 8th
            pygame.draw.rect(screen,(color),(orig[0]+x*blocksize,orig[1]-y*blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+x*-blocksize,orig[1]-y*blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+x*blocksize,orig[1]-y*-blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+x*-blocksize,orig[1]-y*-blocksize,blocksize,blocksize),0)
            
            #need to reflect to get a quarter
            
            

    for y in range (radius-1):
        for x in range (radius-(1+y)):
            xco = x+1+y
            yco = y
            #print("x and y is :"+str(xco)+","+str(yco))   
            
            if array[xco][yco] == 1:
                color = blue
            elif array[xco][yco] == 2:
                color = green
            elif array[xco][yco] == 3:
                color = red
            else:
                color = (255,255,255)
                
                
            #(startx, starty, lengthx, lengthy)
            

            #draw first 8th
            pygame.draw.rect(screen,(color),(orig[0]+xco*blocksize,orig[1]-yco*blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+xco*-blocksize,orig[1]-yco*blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+xco*blocksize,orig[1]-yco*-blocksize,blocksize,blocksize),0)
            pygame.draw.rect(screen,(color),(orig[0]+xco*-blocksize,orig[1]-yco*-blocksize,blocksize,blocksize),0)
            
    
            

            
    
    
        
            
             
    #pygame.draw.polygon(screen,(255,0,0),((200,200),(200,300),(300,300)),0)
    if displayimg==True:
        pygame.display.update()
    
    if save == True:
        if folder == "sink/":
            pygame.image.save(screen,folder+str(cursand)+"_"+str(1+(radius-1)*2)+"x"+str(1+(radius-1)*2)+"_"+folder[:-1]+".png")
        elif folder == "sinkVertice/":
            pygame.image.save(screen,folder+str(cursand)+"_"+str(1+(radius-1)*2)+"x"+str(1+(radius-1)*2)+"_"+folder[:-1]+".png")
        else:
            pygame.image.save(screen,folder+str(cursand)+"_"+folder[:-1]+".png")
        
    #print("Step "+str(cursand)+" out of "+str(terminateAt))
    
    
    
    
    
    
    
    
    
    
    
    
    
    