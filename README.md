## Sandpiles

This is my first project I have made using python as a way of learning. This project was inspired by Numberphile with the video on Abelian Sandpiles which can be seen here: https://www.youtube.com/watch?v=1MtEUErz7Gg

The code is very messy as I am still at a very early stage of coding but the code does run and output some nice pictures!